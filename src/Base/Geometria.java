package Base;

import java.util.Scanner;

public class Geometria {

	//Calculate the area for a square using one side
	public static int squareArea(int side)
	{
		if(side<0){
			throw new IllegalArgumentException("Valor dado no valido");
		} else{
			int area = (int)Math.pow(side, 2);
			return area;
		}
	}

	//Calculate the area for a square using two sides
	public static int squareArea(int sidea, int sideb)
	{
		if(sidea<0 || sideb<0 ){
			throw new IllegalArgumentException("Valor no valido");
		}
		else
		{
			int area = sidea * sideb;
			return area;
		}
	}

	//Calculate the area of circle with the radius
	public static double circleArea(double radius) {
		if (radius < 0) {
			throw new IllegalArgumentException("Valor dado no valido");
		} else {
			double area = Math.pow(radius, 2) * Math.PI;
			return area;
		}
	}

	//Calculate the perimeter of square with a side
	public static double squarePerimeter(double side)
	{
		if(side < 0)
		{
			throw new IllegalArgumentException();
		}
		else {
			double perimeter = Math.pow(side, 2);
			return perimeter;
		}
	}
	//Calculate the perimeter of circle with the diameter
	public static double circlePerimeter(int diameter)
	{

		double perimeter = Math.PI*diameter;
		return perimeter;
	}

	//Calculate the volume of the sphere with the radius
	public static double sphereVolume(double radius)
	{
		double volume = (4 * Math.PI * Math.pow(radius, 3)/3);
		return volume;
	}

	//Calculate the area of regular pentagon with one side
	public static float pentagonArea(int side)
	{
		float area = (float) ((5*Math.pow(side, 2))/(4*Math.tan(Math.PI/5)));
		return area;
	}

	//Calculate the Hypotenuse with two cathetus
	public static double calculateHypotenuse(double catA, double catB)
	{
		double hypotenuse = Math.sqrt( Math.pow(catA, 2)+ Math.pow(catB, 2));
		return hypotenuse;
	}

	public void hasTrabajo(){
		Scanner input = new Scanner(System.in);
		int numA, numB;
		byte option;
		double numG;

		do {

			System.out.println("\nBienvenido a Geometria elija una opcion \n1.Area Cuadrado \n2.Area del Rectangulo \n3.Area del Circulo \n4.Perimetro Cuadrado" +
					"\n5.Perimetro de un Circulo \n6.Volumen de una Esfera \n7.Area del Pentagono \n8.Hipotenusa \n9.Salir");
			option = input.nextByte();

			switch (option) {
				case 1:
					System.out.println("Ingrese Numero del lado del cuadrado");
					numA = input.nextInt();

					System.out.println("El resultado es: " + Geometria.squareArea(numA));
					break;
				case 2:
					System.out.println("Ingrese Numero del lado 1 del rectangulo");
					numA = input.nextInt();
					System.out.println("Ingrese Numero del lado  2 del rectangulo");
					numB = input.nextInt();

					System.out.println("El resultado es: " + Geometria.squareArea(numA, numB));
					break;
				case 3:
					System.out.println("Ingrese Numero del radio del circulo");
					numG = input.nextDouble();

					System.out.println("El resultado es: " + Geometria.circleArea(numG));
					break;
				case 4:
					System.out.println("Ingrese Numero del lado del cuadrado");
					numG = input.nextDouble();

					System.out.println("El resultado es: " + Geometria.squarePerimeter(numG));
					break;
				case 5:
					System.out.println("Ingrese Numero del diametro del circulo");
					numA = input.nextInt();

					System.out.println("El resultado es: " + Geometria.circleArea(numA));
					break;
				case 6:
					System.out.println("Ingrese Numero del radio de la esfera");
					numG = input.nextDouble();

					System.out.println("El resultado es: " + Geometria.sphereVolume(numG));
					break;
				case 7:
					System.out.println("Ingrese Numero del lado del pentagono");
					numA = input.nextInt();

					System.out.println("El resultado es: " + Geometria.pentagonArea(numA));
					break;
				case 8:
					System.out.println("Ingrese Numero del cateto 1 del ");
					numA = input.nextInt();
					System.out.println("Ingrese Numero del cateto 2 del rectangulo");
					numB = input.nextInt();
					System.out.println("El resultado es: " + Geometria.calculateHypotenuse(numA, numB));
					break;
				case 9:
					System.out.println("Saliendo de geometria");
					System.out.println("////////////////////////////////////////////");
					break;
				default:
					System.out.println("Error");
					break;
			}
		}while (option != 9);
	}
}