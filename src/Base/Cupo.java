package Base;

public class Cupo {
	public static final byte capacidadMin = 0, capacidadMax = 6;
	int cantidadCiclas, cantidadMotos, cantidadCarros;

	public void modCantidadCiclas(int cantidadCiclas) {
		this.cantidadCiclas = cantidadCiclas;
	}
	
	static final byte pesoCarro = 6, pesoMoto = 3, pesoCicla = 1;
	boolean lleno;
	int capacidad = 0;
	
	
	public void sumarCicla () {
		this.cantidadCiclas++;
	}
		
	public void sumarMoto () {
		this.cantidadMotos++;
	}
	
	public void sumarCarro () {
		this.cantidadCarros++;
	}
	
	public void restarCicla () {
		this.cantidadCiclas--;
	}
		
	public void restarMoto () {
		this.cantidadMotos--;
	}
	
	public void restarCarro () {
		this.cantidadCarros--;
	}
	
	public int obtenCantidadCiclas () {
		return cantidadCiclas;
	}
	
	public int obtenCapacidad () {
		return capacidad;
	}
	
	public void modCapacidad (int valor){
		this.capacidad += valor;
	}
	
	public int obtenCantidadMotos () {
		return cantidadMotos;
	}
	public int obtenCantidadCarros () {
		return cantidadCarros;
	}
	
    public boolean estoyLleno () {
    	return capacidad == pesoCarro;
    }
    
    public boolean estoyVacio () {
    	return capacidad == 0;
    }
}
