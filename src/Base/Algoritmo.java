package Base;

import java.util.Scanner;

public class Algoritmo {
    static Scanner input=new Scanner(System.in);
    static Agilidad agil=new Agilidad();
    static Conversion conver=new Conversion();
    static Geometria geo=new Geometria();
    static byte option;

    public void hasTrabajo(){

        do {

            System.out.println("Bienvenido a Algoritmos \nelija una opcion \n1.Agilidad \n2.Conversiones \n3.Geometria \n4.Salir");
            option=input.nextByte();

            switch (option) {
                case 1:
                    agil.hasTrabajo();
                    break;
                case 2:
                    conver.hasTrabajo();
                    break;
                case 3:
                    geo.hasTrabajo();
                    break;
                case 4:
                    System.out.println("Saliendo de algoritmos");
                    System.out.println("/////////////////////////////");
                    break;
            }
        }while (option != 4);
    }
}

