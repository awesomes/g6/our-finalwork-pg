package Base;

import java.util.Scanner;

public class Agilidad {

	//bigger than

	//Show if the first number is bigger than the second
	public static boolean biggerThan(String numA, String numB)
	{
		double val1 = Double.parseDouble(numA);
		double val2 = Double.parseDouble(numB);
		if(val1>val2){
			return true;

		}
		else
		{
			return false;
		}
	}

	//Sort from bigger the numbers an show in array
	public static int[] order(int numA, int numB, int numC,int numD,int numE)
	{
		int[] numbers=new int[5];
		numbers[0]=numA;
		numbers[1]=numB;
		numbers[2]=numC;
		numbers[3]=numD;
		numbers[4]=numE;

		int i, j, aux;
		for (i = 0; i < numbers.length - 1; i++) {
			for (j = 0; j < numbers.length - i - 1; j++) {
				if (numbers[j + 1] < numbers[j]) {
					aux = numbers[j + 1];
					numbers[j + 1] = numbers[j];
					numbers[j] = aux;
				}
			}
		}
		for(int f = 0; f < numbers.length; f++)
		{
			// Imprimimos los elementos del array en pantalla.
			System.out.print(""+numbers[f]+ ", ");
		}
		return numbers;
	}

	//Look for the smaller number of the array
	public static int smallerThan(int numA, int numB, int numC,int numD,int numE) {

		int[] numbers=new int[5];
		numbers[0]=numA;
		numbers[1]=numB;
		numbers[2]=numC;
		numbers[3]=numD;
		numbers[4]=numE;

		int menor, tmp;
		int i, j, pos;
		for (i = 0; i < numbers.length - 1; i++) {
			menor = numbers[i];
			pos = i; //
			for (j = i + 1; j < numbers.length; j++) {
				if (numbers[j] < menor) {
					menor = numbers[j];
					pos = j;
				}
			}
			if (pos != i) {
				tmp = numbers[i];
				numbers[i] = numbers[pos];
				numbers[pos] = tmp;
			}
		}
		return numbers[0];
	}

	//Palindrome number is called in Spanish capicúa
	//The number is palindrome
	public static boolean palindromeNumber(Integer numA)
	{
		int aux, num,invert = 0;
		aux = numA;
		while (aux!=0){
			num = aux % 10;
			invert = invert * 10 + num;
			aux = aux / 10;
		}

		if(numA == invert){
			return true;
		}else{
			return false;
		}
	}



	//the word is palindrome
	public static boolean palindromeWord(String word)
	{
		String pal = "";
		for (int i=word.length()-1;i >= 0;i--){
			pal = pal + word.charAt(i);
		}
		if (word.equals(pal)){
			return true;
		}
		return false;
	}


	//Show the factorial number for the parameter
	public static int factorial(int numA)
	{
		int resultado=1;
		if (numA == 0) {
			return 0;

		}else{
			for (int i = 1; i <= numA; i++) {
				resultado *= i;
			}
		}
		return resultado;
	}

	//is the number odd
	public static boolean isOdd(byte numA)
	{
		if(numA%2 == 0){
			return false;
		}else{
			return true;
		}
	}

	//is the number prime
	public static boolean isPrimeNumber(int numA)
	{
		int i,contador=0;
		for(i=1; i<=numA; i++){
			if((numA % i) == 0){
				contador++;
			}
		}

		if(contador == 2)
		{
			return true;
		}else{
			return false;
		}
	}

	//is the number even
	public static boolean isEven(byte numA)
	{
		if(numA%2 == 0){
			return true;
		}else{
			return false;
		}
	}

	//is the number perfect
	public static boolean isPerfectNumber(int numA) {
		int i, sum=0;
		for (i = 1; i < numA; i++) {
			if (numA % i == 0) {
				sum = sum + i;
			}
		}
		if (sum == numA) {
			return true;
		} else {
			return false;

		}
	}

	//Return an array with the fibonacci sequence for the requested number
	public static int [] fibonacci(int numA)
	{
		int[] fiboArray = new int[numA+1];
		fiboArray[0] = 0;

		for(int i = 0; i < numA;i++)
			fiboArray[i+1] = recursiveFibonacci(i);

		return fiboArray;

	}
	public static int recursiveFibonacci (int num){
		return(num == 0 || num == 1)?1:recursiveFibonacci(num-2) + recursiveFibonacci(num-1);
	}


	//how many times the number is divided by 3
	public static int timesDividedByThree(int numA) {
		int suma = 3, contador = 0;
		if (numA < 3) {
			throw new IllegalArgumentException("El numero debe ser mayor a 3");
		}else if(numA ==3){
			return 1;
		}
		else  {
			while (numA >= suma) {
				suma = suma + 3;
				contador++;
			}
			return contador;
		}
	}

	//The game of fizzbuzz
	public static String fizzBuzz(int numA)
	/**
	 * If number is divided by 3, show fizz
	 * If number is divided by 5, show buzz
	 * If number is divided by 3 and 5, show fizzbuzz
	 * in other cases, show the number
	 */


	{
		String fizz = "Fizz";
		String buzz ="Buzz";
		String fizzbuzz ="FizzBuzz";
		String enteroString = Integer.toString(numA);

		if(numA%5 == 0 && numA%3 == 0){
			return fizzbuzz;
		} else if(numA%5 == 0){
			return buzz;
		} else if(numA%3 == 0){
			return fizz;
		} else {
			return enteroString;
		}
	}

	public void hasTrabajo() {

		Scanner input = new Scanner(System.in);
		int numA, numB, numC, numD, numE;
		byte option, numF;
		String word1, word2;

		do {

			System.out.println("\nBienvenido a Agilidad elija un opcion \n1.Bigger Than \n2.Sort from Bigger \n3.Smaller number \n4.Palindrome number \n5.Palindrome word \n6.Factorial number \n7.Number odd" +
					" \n8.Number prime \n9.Number even \n10.Number perfect \n11.Fibonacci \n12.Number divided by 3 \n13.Game fizzbuzz \n14.Salir");
			option = input.nextByte();
			if (option != 0) {

				switch (option) {
					case 1:
						System.out.println("Ingrese Numero 1");
						word1 = input.next();
						System.out.println("Ingrese Numero 2");
						word2 = input.next();

						System.out.println("El resultado es: " + Agilidad.biggerThan(word1, word2));
						break;
					case 2:
						System.out.println("Ingrese 5 Numeros");
						System.out.println("Ingrese Numero 1");
						numA = input.nextInt();
						System.out.println("Ingrese Numero 2");
						numB = input.nextInt();
						System.out.println("Ingrese Numero 3");
						numC = input.nextInt();
						System.out.println("Ingrese Numero 4");
						numD = input.nextInt();
						System.out.println("Ingrese Numero 5");
						numE = input.nextInt();

						Agilidad.order(numA, numB, numC, numD, numE);
						break;
					case 3:
						System.out.println("Ingrese 5 Numeros");
						System.out.println("Ingrese Numero 1");
						numA = input.nextInt();
						System.out.println("Ingrese Numero 2");
						numB = input.nextInt();
						System.out.println("Ingrese Numero 3");
						numC = input.nextInt();
						System.out.println("Ingrese Numero 4");
						numD = input.nextInt();
						System.out.println("Ingrese Numero 5");
						numE = input.nextInt();

						System.out.println("El numero menor es: " + Agilidad.smallerThan(numA, numB, numC, numD, numE));
						break;
					case 4:
						System.out.println("Ingrese su numero a verificar");
						numA = input.nextInt();

						System.out.println("El resultado es: " + Agilidad.palindromeNumber(numA));
						break;
					case 5:
						System.out.println("Ingrese su palabra a verificar");
						word1 = input.next();

						System.out.print("El resultado es: " + Agilidad.palindromeWord(word1));
						break;
					case 6:
						System.out.println("Ingrese su numero a hacer proceso Factorial");
						numA = input.nextInt();

						System.out.println(Agilidad.factorial(numA));
						break;
					case 7:
						System.out.println("Ingrese Numero");
						numF = input.nextByte();
						System.out.print("El resultado es: " + Agilidad.isOdd(numF));
						break;
					case 8:
						System.out.println("Ingrese numero a Valorar");
						numA = input.nextInt();
						System.out.println("El resultado es: " + Agilidad.isPrimeNumber(numA));
						break;
					case 9:
						System.out.println("Ingrese numero a Valorar");
						numF = input.nextByte();
						System.out.println("El resultado es: " + Agilidad.isEven(numF));
						break;
					case 10:
						System.out.println("Ingrese numero a Valorar");
						numA = input.nextInt();
						System.out.println("El resultado es: " + Agilidad.isPerfectNumber(numA));
						break;
					case 11:
						System.out.println("Ingrese numero a realizar serie Fibonacci");
						numA = input.nextInt();
						System.out.println(Agilidad.fibonacci(numA));
						break;
					case 12:
						System.out.println("Ingrese numero a dividir por tres");
						numA = input.nextInt();
						System.out.println("El resultado es: " + Agilidad.timesDividedByThree(numA));
						break;
					case 13:
						System.out.println("Ingrese numero a jugar Fizzbuzz");
						numA = input.nextInt();
						System.out.println("El resultado es: " + Agilidad.fizzBuzz(numA));
						break;
					case 14:
						System.out.println("Saliendo de agilidad");
						System.out.println("////////////////////////////////////////////");
						break;
					default:
						System.out.println("Error");
						break;
				}
			}
		} while (option != 14);
	}
}