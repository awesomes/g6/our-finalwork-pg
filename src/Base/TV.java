package Base;

import java.util.Scanner;

public class TV {
	static Scanner input=new Scanner(System.in);
	static String on = "E", onusername = "";
	static byte option;

	public void hasTrabajo() {
		System.out.println("//////////////////////////////////////////////////////");
		System.out.println("Bienvenido a la television");
		System.out.println("Manual de usuario");
		System.out.println("Pulse la letra (E) para encender la TV");
		System.out.println("Para desplasarse entre volumen solo es necesario oprimir algun numero (1 - 100) lo mismo para canales (1 - 25)");
		System.out.println("Para silenciar el tv oprima 0");
		System.out.println("/////////////////////////////////////////////////////");
		while (on.equals(onusername) == false) {
			System.out.println("Encienda la tv");
			onusername = input.nextLine();
			if (on.equals(onusername) == false) {
				System.out.println("Boton incorrecto, siga la instruccion");
				System.out.println("/////////////////////////////////////////////");
			}
		}
		do {
			System.out.println("/////////////////////////////////////////////////////");
			System.out.println("Encendido \nSource \n1.TV \n2.HDMI \n3.AV \n4.Apagar");
			option = input.nextByte();

			switch (option) {
				case 1:
					System.out.println("//////////////////////////////////////////////");
					System.out.println("TV");
					do {
						System.out.println("1.Volumen \n2.Canales \n3.Source");
						option = input.nextByte();
						System.out.println("//////////////////////////////////////////////");

						switch (option) {
							case 1:
								volumeupanddown();
								break;
							case 2:
								changechannel();
								break;
							case 3:
								System.out.println("Source");
								break;
							default:
								System.out.println("Error");
								break;
						}
					}while (option != 3);
					break;

				case 2:
					System.out.println("/////////////////////////////////////////////");
					System.out.println("HDMI");
					do {
						System.out.println("1.Volumen \n2.Canales \n3.Source");
						option = input.nextByte();
						System.out.println("//////////////////////////////////////////////");

						switch (option) {
							case 1:
								volumeupanddown();
								break;
							case 2:
								System.out.println("En este formato no se encuentra disponibles esta opcion");
								System.out.println("//////////////////////////////////////////////");
								break;
							case 3:
								System.out.println("Source");
								break;
							default:
								System.out.println("Error");
								break;
						}
					}while (option != 3);
					break;

				case 3:
					System.out.println("//////////////////////////////////////");
					System.out.println("AV");
					do {
						System.out.println("1.Volumen \n2.Canales \n3.Source");
						option = input.nextByte();
						System.out.println("//////////////////////////////////////////////");

						switch (option) {
							case 1:
								volumeupanddown();
								break;
							case 2:
								System.out.println("En este formato no se encuentra disponibles esta opcion");
								System.out.println("///////////////////////////////////////////////////////");
								break;
							case 3:
								System.out.println("Source");
								break;
							default:
								System.out.println("Error");
								break;
						}
					}while (option != 3);
					break;

				case 4:
					break;
			}
		}while (option != 4);
		//Regresa al menu principal
	}

	//Metodo de subir, bajar volumen y mute.
	public void volumeupanddown(){
		byte volume=1;

		do {
			System.out.println("Vol \n" + volume);
			volume=input.nextByte();
			System.out.println("-----------------------------------");
			if (volume == 0){
				System.out.println("Sin sonido");
				System.out.println("-----------------------------------");
			}
		}while ((volume>=0)&&(volume<=100));
	}

	//Metodo de cambiar de canal
	public void changechannel(){
		byte channel=1;

		do {
			System.out.println("Channel \n" + channel);
			channel=input.nextByte();
			System.out.println("-----------------------------------");
		}while ((channel>=1)&&(channel<=25));
	}
}