package Base;

import java.util.Scanner;

public class Conversion {

	//convert of units of Metric System

	//Km to metters
	public static int kmToM1(double km)
	{
		double result = km*1000;
		int operation = (int)result;
		return  operation;
	}

	//Km to metters
	public static double kmTom(double km)
	{
		double result = km*1000;
		return  result;
	}

	//Km to cm
	public static double kmTocm(double km)
	{
		double result = km*100000;
		return  result;
	}

	//milimetters to metters
	public static double mmTom(int mm)
	{
		double result = mm*0.001;
		return  result;
	}
//convert of units of U.S Standard System

	//convert miles to foot
	public static double milesToFoot(double miles)
	{
		double result = miles*5280;
		return result;
	}

	//convert yards to inches
	public static int yardToInch(int yard)
	{
		int result = yard*36;
		return result;
	}

	//convert inches to miles
	public static double inchToMiles(double inch)
	{
		double result = inch/63360;
		return result;
	}
	//convert foot to yards
	public static int footToYard(int foot)
	{
		int result = foot/3;
		return result;
	}

//Convert units in both systems

	//convert Km to inches
	public static double kmToInch(int km)
	{
		double operation =km;
		double result = operation*39370.079;
		return result;
	}

	//convert milimmeters to foots
	public static double mmToFoot(int mm)
	{
		double operation =mm;
		double result = operation/304.8;
		return result;
	}
	//convert yards to cm
	public static double yardToCm(int yard)
	{
		double operation =yard;
		double result = operation*91.44;
		return result;
	}

	public void hasTrabajo(){
		Scanner input=new Scanner(System.in);
		int numA;
		byte option;
		double numG;

		do {

			System.out.println("\nBienvenido a Conversion \n1.Kilometros a Metros \n2.Kilometros a Centimetros \n3.Milimietros a metros \n4.Millas a Pies" +
					"\n5.Yardas a Pulgadas \n6.Pulgadas a Millas \n7.Pies a Yardas \n8.Kilometros a Pulgadas \n9.Milimetros a Pies \n10.Yardas a Centimetros \n11.Salir");
			option = input.nextByte();

			switch (option) {
				case 1:
					System.out.println("Ingrese Numero de Kilometros a convertir");
					numG = input.nextDouble();

					System.out.println("El resultado es: " + Conversion.kmTom(numG) + " metros");
					break;
				case 2:
					System.out.println("Ingrese Numero de Kilometros a convertir");
					numG = input.nextDouble();

					System.out.println("El resultado es: " + Conversion.kmTocm(numG) + " centimetros");
					break;
				case 3:
					System.out.println("Ingrese Numero de milimetros a convertir");
					numA = input.nextInt();

					System.out.println("El resultado es: " + Conversion.mmTom(numA) + " metros");
					break;
				case 4:
					System.out.println("Ingrese Numero de millas a convertir");
					numG = input.nextDouble();

					System.out.println("El resultado es: " + Conversion.milesToFoot(numG) + " pies");
					break;
				case 5:
					System.out.println("Ingrese Numero de Yardas a convertir");
					numA = input.nextInt();

					System.out.println("El resultado es: " + Conversion.yardToInch(numA) + " Pulgadas");
					break;
				case 6:
					System.out.println("Ingrese Numero de Pulgadas a convertir");
					numG = input.nextDouble();

					System.out.println("El resultado es: " + Conversion.inchToMiles(numG) + " Millas");
					break;
				case 7:
					System.out.println("Ingrese Numero de Pies a convertir");
					numA = input.nextInt();

					System.out.println("El resultado es: " + Conversion.footToYard(numA) + " Yardas");
					break;
				case 8:
					System.out.println("Ingrese Numero de Kilometros a convertir");
					numA = input.nextInt();

					System.out.println("El resultado es: " + Conversion.kmToInch(numA) + " Pulgadas");
					break;
				case 9:
					System.out.println("Ingrese Numero de mmilimetros a convertir");
					numA = input.nextInt();

					System.out.println("El resultado es: " + Conversion.mmToFoot(numA) + " Pies");
					break;
				case 10:
					System.out.println("Ingrese Numero de yardas a convertir");
					numA = input.nextInt();

					System.out.println("El resultado es: " + Conversion.yardToCm(numA) + " centimetros");
					break;
				case 11:
					System.out.println("Saliendo de conversion");
					System.out.println("////////////////////////////////////////////");
					break;
				default:
					System.out.println("Error");
					break;
			}
		}while (option != 11);
	}

}