package Base;

import java.util.Scanner;

public class Parqueadero {
	
	static Scanner  entrada = new  Scanner(System.in);
	static Cupo[] cupos = {new Cupo(), 
						   new Cupo(),
						   new Cupo(),
						   new Cupo(),
						   new Cupo()};
	
	public void hasTrabajo() {
		byte option;
		do{
			
			System.out.println("Bienvenido a parquedero, que desea hacer: ");
			System.out.println("1. Mostrar informaci�n de cupos");
			System.out.println("2. A�adir un vehiculo");
			System.out.println("3. Eliminar un vehiculo");
			System.out.println("4. Salir del parqueadero");
			
			option = entrada.nextByte();
			switch (option) {
				case 1:
					imprimirCupos();
					break;
				case 2:
					a�adirVehiculo();
					break;
				case 3:
					eliminarVehiculo();
					break;
				case 4:
					System.out.println("Saliendo del sistema de parqueadero...");
					break;
					default:
						System.out.println("Entrada incorrecta");
						break;
		    	}
		}while (option != 4);
	}
	
	static void a�adirVehiculo () {
		byte indexadorDeCupo, tipoVehiculo;
		
		do {
			System.out.println("Seleccione el cupo de un rango de 1 a 5");
			indexadorDeCupo = entrada.nextByte();
			
			if(indexadorDeCupo<1 || indexadorDeCupo > 5)
				System.out.println("Valor ingresado fuera de rango\n");
			
		}while(indexadorDeCupo<1 || indexadorDeCupo > 5);
		
		indexadorDeCupo--;
		if(cupos[indexadorDeCupo].estoyLleno()){
			System.out.println("El cupo seleccionado est� lleno.\n Regresando al men� de parquedero");
			return;
		}
		
		do {
			
			System.out.println("Ingrese el tipo de vehiculo a agregar");
			System.out.println("1. Cicla");
			System.out.println("2. Moto");
			System.out.println("3. Carro");
			
			tipoVehiculo = entrada.nextByte();
			
			if(tipoVehiculo <1 || tipoVehiculo > 3)
				System.out.println("Valor ingresado fuera de rango\n");
			
		}while(tipoVehiculo <1 || tipoVehiculo > 3);
		
		
		switch(tipoVehiculo){
		case 1:
			if((cupos[indexadorDeCupo].obtenCapacidad() + Cupo.pesoCicla)> Cupo.pesoCarro){
				System.out.println("No se puede hacer la dicion adici�n se sobrepasaria el cupo");
				return;
			}else{
				cupos[indexadorDeCupo].sumarCicla();
				cupos[indexadorDeCupo].modCapacidad(Cupo.pesoCicla);
				System.out.println("Acci�n exitosa");
			}
			
			break;
		case 2:
			if((cupos[indexadorDeCupo].obtenCapacidad() + Cupo.pesoMoto)> Cupo.pesoCarro){
				System.out.println("No se puede hacer la adici�n porque se sobrepasaria el cupo");
				return;
			}else{
				cupos[indexadorDeCupo].sumarMoto();
				cupos[indexadorDeCupo].modCapacidad(Cupo.pesoMoto);
				System.out.println("Acci�n exitosa");
			}
			
			break;
		case 3:
			if((cupos[indexadorDeCupo].obtenCapacidad() + Cupo.pesoCarro)> Cupo.pesoCarro){
				System.out.println("No se puede hacer la adici�n porque se sobrepasaria el cupo");
				return;
			}else{
				cupos[indexadorDeCupo].sumarCarro();
				cupos[indexadorDeCupo].modCapacidad(Cupo.pesoCarro);
				System.out.println("Acci�n exitosa");
			}
			break;
		}
		System.out.println("Retornando al menu de parqueadero");
	}
	
	static void eliminarVehiculo () {
    byte indexadorDeCupo, tipoVehiculo;
		
		do {
			System.out.println("Seleccione el cupo de un rango de 1 a 5");
			indexadorDeCupo = entrada.nextByte();
			
			if(indexadorDeCupo<1 || indexadorDeCupo > 5)
				System.out.println("Valor ingresado fuera de rango\n");
			
		}while(indexadorDeCupo<1 || indexadorDeCupo > 5);
		indexadorDeCupo--;
		if(cupos[indexadorDeCupo].estoyVacio()){
			System.out.println("El cupo seleccionado est� vacio.\n Regresando al men� de parquedero");
			return;
		}
		
		do {
			
			System.out.println("Ingrese el tipo de vehiculo a eliminar");
			System.out.println("1. Cicla");
			System.out.println("2. Moto");
			System.out.println("3. Carro");
			
			tipoVehiculo = entrada.nextByte();
			
			
			if(indexadorDeCupo <1 || indexadorDeCupo > 3)
				System.out.println("Valor ingresado fuera de rango\n");
			
		}while(indexadorDeCupo <1 || indexadorDeCupo > 3);
		
		switch(tipoVehiculo){
		case 1:
			if((cupos[indexadorDeCupo].obtenCantidadCiclas() < 1)){
				System.out.println("No se puede eliminar este tipo de behiculo porque no hay");
				return;
			}else{
				cupos[indexadorDeCupo].restarCicla();
				cupos[indexadorDeCupo].modCapacidad(-Cupo.pesoCicla);
				System.out.println("Acci�n exitosa");
			}
			
			break;
		case 2:
			if((cupos[indexadorDeCupo].obtenCantidadMotos() < 1)){
				System.out.println("No se puede eliminar este tipo de behiculo porque no hay");
				return;
			}else{
				cupos[indexadorDeCupo].restarMoto();
				cupos[indexadorDeCupo].modCapacidad(-Cupo.pesoMoto);
				System.out.println("Acci�n exitosa");
			}
			
			break;
		case 3:
			if((cupos[indexadorDeCupo].obtenCantidadCarros() < 1)){
				System.out.println("No se puede eliminar este tipo de behiculo porque no hay");
				return;
			}else{
				cupos[indexadorDeCupo].restarCarro();
				cupos[indexadorDeCupo].modCapacidad(-Cupo.pesoCarro);
				System.out.println("Acci�n exitosa");
			}
			break;
		}
		System.out.println("Retornando al menu de parqueadero");
		
	}
	
	static void imprimirCupos () {
		String estado;
		for(int i = 0; i < cupos.length; i++){
			
			estado = (cupos[i].capacidad > 0) ?cupos[i].estoyLleno() ? "lleno": "Usado con espacio":"vacio";
			System.out.println("El Cupo  numero " + (i +1) + " tiene:");
			
			System.out.println("Carros: " + cupos[i].obtenCantidadCarros());
			System.out.println("Motos: " + cupos[i].obtenCantidadMotos());
			System.out.println("Ciclas: " + cupos[i].obtenCantidadCiclas());
			System.out.println("Estado: " + estado);
			System.out.println("\n");
		}
		
	}
}
