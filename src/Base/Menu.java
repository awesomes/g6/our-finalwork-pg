package Base;

import java.util.Scanner;

public class Menu {
	private static Scanner input=new Scanner(System.in);
	private static byte option;
	private static Algoritmo algoritmo=new Algoritmo();
	private static Parqueadero parqueadero=new Parqueadero();
	private static Loteria loteria=new Loteria();
	private static TV tv=new TV();

	public static void main(String[] args) {

		do {
			Menu();
		}while (option != 5);
		System.out.println("Programa terminado");
	}


	static void Menu() {

		input = new Scanner(System.in);

		System.out.println("\tBienvenido al trabajo final \nelija una opcion  \n1.Algortimos \n2.Parqueadero \n3.Loteria \n4.Television \n5.Salir");
		option = input.nextByte();

		switch (option) {
			case 1:
				algoritmo.hasTrabajo();
				break;
			case 2:
				parqueadero.hasTrabajo();
				break;
			case 3:
				loteria.hasTrabajo();
				break;
			case 4:
				tv.hasTrabajo();
				break;
			case 5:
				System.out.println("Saliendo del programa");
				break;
			default:
				System.out.println("Valor ingresado incorrecto");
				break;
		}
	}
}
